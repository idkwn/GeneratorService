//
//  File.swift
//  
//
//  Created by admin on 26.05.2023.
//

import Foundation

class EndRoutes {
    
    // MARK: - main end
    
    static let END_TOKEN = "token"
    static let END_APPLICATION_NAME = "application_name"
    static let END_NAME = "name"
    static let END_SCREEN_ORIENTATION = "screen_orientation"
    static let END_COLOOR = "color"
    static let END_PACKAGE_NAME = "package_name"
    static let END_PACKAGE_NAME_MODULE = "package_name_module"
    static let END_GRADLE_VERSION = "gradle_version"
    static let END_COMPILE_SDK = "compile_sdk"
    static let END_MIN_SDK = "min_sdk"
    static let END_TARGET_SDK = "target_sdk"
    static let END_KOTLIN = "kotlin"
    static let END_KOTLIN_COROUTINES = "kotlin_coroutines"
    static let END_HILT = "hilt"
    static let END_HILT_VIEWMODEL_COMPILER = "hilt_viewmodel_compiler"
    static let END_KTX = "ktx"
    static let END_LIFECYCLE = "lifecycle"
    static let END_FRAGMENT_KTX = "fragment_ktx"
    static let END_APPCOMPAT = "appcompat"
    static let END_MATERIAL = "material"
    static let END_COMPOSE = "compose"
    static let END_COMPOSE_NAVIGATION = "compose_navigation"
    static let END_ACTIVITY_COMPOOSE = "activity_compose"
    static let END_COMPOSE_HILT_NAV = "compose_hilt_nav"
    static let END_ONESIGNAL = "onesignal"
    static let END_GLIDE = "glide"
    static let END_SWIPE = "swipe"
    static let END_GLIDE_SKYDOVES = "glide_skydoves"
    static let END_RETROFIT = "retrofit"
    static let END_OKHTTP = "okhttp"
    static let END_ROOM = "room"
    static let END_COIL = "coil"
    static let END_EXP = "exp"
    static let END_CALEND = "calend"
    static let END_PAGING = "paging"
    static let END_KOIN = "koin"
    static let END_KOIN_COMPOSE = "koin_compose"
    static let END_KOIN_ANNOTATIONS = "koin_annotations"
    static let END_KOTLIN_COMPILER_EXTENSION = "kotlin_compiler_extension"
    static let END_GRADLE_WRAPPER = "gradle_wrapper"
    static let END_HOME_DIR = "home_dir"
    static let END_APP_ID = "app_id"
    static let END_APP_PREFIX = "app_prefix"
    static let END_ACCOMPANIST = "accompanist"
    
    // MARK: - defaults
    
    static let DEFAULT_HOOME_DIR = "/Users/admin/"
    static let DEFAULT_APPLICATION_NAME = "Application"
    static let DEFAULT_APP_NAME = "MyApplication"
    static let DEFAULT_COLOR = "003df0"
    static let DEFAULT_PACKAGE_NAME = "com.example.myapplication"
    static let DEFAULT_PACKAGE_NAME_MODULE = "com.example"
    static let DEFAULT_COLOR_WHITE = "FFFFFF"
    
    
    // MARK: - app variable end
    
    static let END_BACK_COLOR_PRIMARY = "back_color_primary"
    static let END_BACK_COLOR_SECONDARY = "back_color_secondary"
    static let END_SURFACE_COLOR = "surface_color"
    static let END_ON_SURFACE_COLOR = "on_surface_color"
    static let END_PRIMARY_COLOR = "primary_color"
    static let END_ON_PRIMARY_COLOR = "on_primary_color"
    static let END_ERROR_COLOR = "error_color"
    static let END_TEXT_COLOR_PRIMARY = "text_color_primary"
    static let END_TEXT_COLOR_SECONDARY = "text_color_secondary"
}
